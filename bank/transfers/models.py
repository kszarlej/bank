from __future__ import unicode_literals

from django.db import models


class History(models.Model):

    from_account = models.ForeignKey('accounts.Account', related_name='history_to', on_delete=models.CASCADE)
    to_account = models.ForeignKey('accounts.Account', related_name='history_from', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    amount = models.IntegerField()
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}:{}'.format(self.title, self.amount)
