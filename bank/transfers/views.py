from django.db.models import Q
from .serializers import TransferSerializer, HistorySerializer
from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated
from .models import History


class TransferViewSet(mixins.CreateModelMixin,
                      viewsets.GenericViewSet):

    serializer_class = TransferSerializer
    permission_classes = (IsAuthenticated,)
    queryset = None


class HistoryViewSet(viewsets.ReadOnlyModelViewSet):

    serializer_class = HistorySerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return History.objects.filter(
            Q(from_account__user=self.request.user) | Q(to_account__user=self.request.user)
        ).order_by('-date')
