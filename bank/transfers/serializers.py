from django.db import transaction
from rest_framework import serializers

from bank.accounts.models import Account
from bank.accounts.serializers import AccountSerializer
from .models import History


class TransferSerializer(serializers.Serializer):

    from_account = serializers.CharField()
    to_account = serializers.CharField()
    amount = serializers.IntegerField()
    title = serializers.CharField()

    class Meta:
        fields = ('from_account', 'to_account', 'amount')

    def validate_amount(self, amount):
        if amount < 1:
            raise serializers.ValidationError("Minimal amount of cash per transaction is 1")
        return amount

    def validate(self, data):
        to_account_qs = Account.objects.filter(account_number=data['to_account'])
        from_account_qs = Account.objects.filter(account_number=data['from_account'])
        amount = data['amount']

        from_qscount = from_account_qs.count()
        if from_qscount == 0:
            raise serializers.ValidationError('Wrong sender account number.')
        elif from_qscount < 0 or from_qscount > 1:
            raise serializers.ValidationError('Internal bank error. Please report this.')

        to_qscount = to_account_qs.count()
        if to_qscount == 0:
            raise serializers.ValidationError('Wrong receiver account number.')
        elif to_qscount < 0 or to_qscount > 1:
            raise serializers.ValidationError('Internal bank error. Please report this.')

        current_balance = from_account_qs.first().current_amount
        if current_balance < amount:
            raise serializers.ValidationError('Transfered money amount is bigger than account balance.')

        return data

    @transaction.atomic
    def create(self, validated_data):
        to_account = Account.objects.filter(account_number=validated_data.get('to_account')).first()
        from_account = Account.objects.filter(account_number=validated_data.get('from_account')).first()
        amount = validated_data.get('amount')
        title = validated_data.get('title')

        from_account.current_amount -= amount
        from_account.save()

        to_account.current_amount += amount
        to_account.save()

        transfer_history = History()
        transfer_history.from_account = from_account
        transfer_history.to_account = to_account
        transfer_history.amount = amount
        transfer_history.title = title
        transfer_history.save()

        return transfer_history


class HistorySerializer(serializers.ModelSerializer):

    from_account = AccountSerializer()
    to_account = AccountSerializer()

    class Meta:
        model = History
        fields = ('id', 'from_account', 'to_account', 'amount', 'date', 'title')
