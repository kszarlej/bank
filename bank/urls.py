"""bank URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

from users.views import UserViewSet
from accounts.views import AccountViewSet, LimitViewSet
from transfers.views import TransferViewSet, HistoryViewSet


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'accounts', AccountViewSet, base_name='Account')
router.register(r'limits', LimitViewSet, base_name='Limit')
router.register(r'history', HistoryViewSet, base_name='TransferHistory')
router.register(r'transfer', TransferViewSet, base_name='Transfer')


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url('^', include(router.urls), name='api'),
    url(r'^login', obtain_jwt_token),
]
