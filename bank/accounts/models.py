from __future__ import unicode_literals

from django.db import models


class Limit(models.Model):
    daily_transactions = models.IntegerField(default=10)
    daily_transactions_sum = models.IntegerField(default=10000)
    weekly_transactions = models.IntegerField(default=70)
    weekly_transactions_sum = models.IntegerField(default=70000)

    def __str__(self):
        return "daily: {0} daily_sum: {1} weekly: {2} weekly_sum: {3}".format(self.daily_transactions,
                                                                              self.daily_transactions_sum,
                                                                              self.weekly_transactions,
                                                                              self.weekly_transactions_sum)


class Account(models.Model):
    account_number = models.CharField(max_length=27)
    account_name = models.CharField(max_length=30)
    limits = models.ForeignKey(Limit, on_delete=models.CASCADE)
    current_amount = models.IntegerField(default=100000)
    user = models.ForeignKey('auth.User', related_name='accounts',
                             on_delete=models.CASCADE, null=False)

    def __str__(self):
        return "{0}({1}) - {2}PLN".format(self.account_name,
                                          self.account_number,
                                          self.current_amount)
