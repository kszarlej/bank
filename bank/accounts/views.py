from .serializers import AccountSerializer, LimitSerializer
from rest_framework import viewsets
from .utils import get_account_number
from rest_framework import permissions

# Create your views here.
from models import Account, Limit


class AccountViewSet(viewsets.ModelViewSet):
    serializer_class = AccountSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        account_number = get_account_number()
        # try until not used account number found.
        while Account.objects.filter(account_number=account_number).exists() is True:
            account_number = get_account_number()

        serializer.save(user=self.request.user,
                        account_number=account_number)

    def get_queryset(self):
        """
        This viewset should return only users accounts.
        """
        return Account.objects.filter(user=self.request.user)


class LimitViewSet(viewsets.ModelViewSet):
    serializer_class = LimitSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        """
        This viewset should return only users limits.
        """
        return Limit.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
