from rest_framework import serializers
from bank.accounts.models import Account, Limit


class LimitSerializer(serializers.ModelSerializer):

    class Meta:
        model = Limit
        fields = ('daily_transactions', 'daily_transactions_sum',
                  'weekly_transactions', 'weekly_transactions_sum')


class AccountSerializer(serializers.ModelSerializer):

    first_name = serializers.ReadOnlyField(source='user.first_name')
    last_name = serializers.ReadOnlyField(source='user.last_name')
    account_number = serializers.ReadOnlyField()
    limits = LimitSerializer()

    class Meta:
        model = Account
        fields = ('id', 'account_number', 'account_name', 'limits',
                  'current_amount', 'first_name', 'last_name',)
