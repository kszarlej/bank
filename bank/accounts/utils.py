from random import randint


def get_account_number():
    return ''.join(["%s" % randint(0, 9) for num in range(0, 27)])
