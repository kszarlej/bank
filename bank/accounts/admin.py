from django.contrib import admin

# Register your models here.

from models import Limit, Account

admin.site.register(Account)
admin.site.register(Limit)
