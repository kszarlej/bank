from rest_framework import serializers
from django.contrib.auth.models import User
from bank.accounts.serializers import AccountSerializer


class UserSerializer(serializers.ModelSerializer):

    accounts = AccountSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'accounts')
