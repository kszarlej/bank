from rest_framework_jwt.utils import jwt_payload_handler


def payload_handler(user):
    payload = jwt_payload_handler(user)
    payload['first_name'] = user.first_name
    payload['last_name'] = user.last_name

    return payload
