Django
psycopg2
MySQL-python
djangorestframework==3.5.2
django_extensions==1.7.4
djangorestframework-jwt==1.9.0
django-cors-headers==2.0.0
