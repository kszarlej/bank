 FROM python:2.7
 ENV PYTHONUNBUFFERED 1
 RUN mkdir /app
 WORKDIR /app
 ADD requirements.txt /app/
 RUN apt-get update && apt-get -y install postgresql-client-9.4
 RUN pip install -r requirements.txt 
 ADD . /app/
